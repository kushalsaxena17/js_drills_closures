module.exports = function cacheFunction(cb) {
    let dictCache = {};
    return function (...val) {
        if (dictCache[val]) {
            return dictCache[val];
        }
        else {
            dictCache[val] = cb(...val);
            return dictCache[val];
        }
    }

}
