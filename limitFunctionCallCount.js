module.exports.limitFunctionCallCount = (cb, num) => {
   
    return function() {
        if(num>0){
         num--;
         return cb();
        }
        else{
            return console.log(null);
        }
    }
 }
 

