const cacheFunction = require("/home/kushal/Desktop/JS_Drills_Closures/cacheFunction.js");

function callFunction(val1, val2) {
    return val1 * val2;
}


const returnedFunction = cacheFunction(callFunction);

console.log(returnedFunction(200, 5));
console.log(returnedFunction(1000, 5));
